<?php
namespace App\Service;

/**
 * @author Philemon Globlehi <philemon.globlehi@gmail.com>
 *
 * Class DiscountCalculation
 * @package App\Service
 */
class DiscountCalculation
{
    public static function getAmountDiscount(int $initialAmount, int $discount): int
    {
        return ($initialAmount * $discount) / 100;
    }
}