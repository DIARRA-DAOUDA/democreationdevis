<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Philemon Globlehi <philemon.globlehi@gmail.com>
 *
 * Class DiscountController
 * @package App\Controller
 */
class DiscountController extends AbstractController
{
    /**
     * @Route("/discount", name="discount")
     */
    public function index(): Response
    {
        return $this->render('discount/index.html.twig', [
            'controller_name' => 'DiscountController',
        ]);
    }
}
